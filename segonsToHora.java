import java.util.Scanner;

public class segonsToHora {
	static Scanner teclado = new Scanner(System.in);
	static int hores, minuts, segons;
	static int numero;
	static String total;
	public static void main(String[] args) {
		conversorSegundos(numero);
		
	}
	public static String conversorSegundos(Integer numero) {
		//Com el nom del programa indica, hem de passar els segons introduits per teclat, i convertir-los a un format de hora.
		int hores,minuts,segons;

		//Demanem el número total de segons, per després convertirlo en el format de hora, fent les operacions següents.
		System.out.println("Numero:");
		numero = teclado.nextInt();
		
		hores = numero/3600;
		
		minuts = (numero-(3600 * hores))/60;
		
		segons = numero -((hores*3600) + (minuts*60));
		
		total = hores + "h : " + minuts +"m : " + segons + "s";
		
		System.out.println(total);
		
		return total;
		
	}
	
}



