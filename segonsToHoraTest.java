import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class segonsToHoraTest {

	@Test
	void test() {
		assertEquals("5h : 56m : 55s" ,segonsToHora.conversorSegundos(21415));
		assertEquals("3h : 25m : 45s" ,segonsToHora.conversorSegundos(12345));
		assertEquals("0h : 11m : 30s" ,segonsToHora.conversorSegundos(690));
		assertEquals("1h : 8m : 43s" ,segonsToHora.conversorSegundos(4123));
		assertEquals("2h : 30m : 51s" ,segonsToHora.conversorSegundos(9051));
		assertEquals("0h : 50m : 32s" ,segonsToHora.conversorSegundos(3032));
		assertEquals("1h : 37m : 3s" ,segonsToHora.conversorSegundos(5823));
		assertEquals("6h : 43m : 4s" ,segonsToHora.conversorSegundos(24184));
		assertEquals("2h : 46m : 32s" ,segonsToHora.conversorSegundos(9992));
		assertEquals("0h : 3m : 40s" ,segonsToHora.conversorSegundos(220));
				
		
	}

}
